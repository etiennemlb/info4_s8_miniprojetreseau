#include <FLUX/muxedsock.h>
#include <FLUX/packet.h>

#include <boost/bind.hpp>
#include <cstdint>
#include <iostream>

#ifndef EXAMPLE1_H
    #define EXAMPLE1_H

class Echo {
public:
    Echo()
        : the_muxed_sock_{}
        , the_channel_{} {
    }

    void operator()(uint16_t                             the_channel,
                    flux::MuxedSock&                     the_muxed_sock,
                    flux::MuxedSock::OnDataCallbackType& the_data_cb) {
        std::cout << "New channel " << the_channel << std::endl;

        the_muxed_sock_ = &the_muxed_sock;
        the_channel_    = the_channel;

        the_data_cb = boost::bind(&Echo::OnData,
                                  this,
                                  boost::placeholders::_1,
                                  boost::placeholders::_2);
    }

private:
    void OnData(flux::Packet*  the_msg,
                flux::SizeType the_data_len) {
        std::cout << "Data len " << the_data_len << '\n';
        std::cout << "Data: \"" << the_msg->GetPayloadData() << "\"\n";
        std::cout << "Echoing" << std::endl;
        the_muxed_sock_->Send(the_channel_,
                              the_msg,
                              the_data_len);
    }

    flux::MuxedSock* the_muxed_sock_;
    uint16_t         the_channel_;
};

#endif