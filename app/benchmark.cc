#include "benchmark.h"

#include <FLUX/acceptor.h>
#include <FLUX/packet.h>

#include <boost/asio.hpp>
#include <chrono>
#include <ctime>
#include <iostream>
#include <mutex>

using boost::asio::ip::tcp;
using boost::asio::ip::udp;
using namespace std::chrono_literals;

////////////////////////////////////////////////////////////////////////////////


#define BUFFERSIZE 4096

static unsigned char the_data[BUFFERSIZE];

////////////////////////////////////////////////////////////////////////////////

static void PrintThr(const std::string& benchname,
                     std::size_t        data_transmitted,
                     std::size_t        time_as_ns) {
    std::cout << benchname
              << ';' << data_transmitted << ';' << time_as_ns << ';' << ((data_transmitted / (1024.0 * 1024.0)) / (time_as_ns / (1E9))) << ';' << (8 * ((data_transmitted / (1024.0 * 1024.0)) / (time_as_ns / (1E9)))) << '\n';
}

static void HandlePacketLoss(uint32_t nb_lost_packets, flux::MuxedSock sock) {
    std::cout << "WARNING: " << nb_lost_packets << " sent packets were lost" << std::endl;
}

static uint64_t GetCurrentTimeStamp() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

static uint64_t GetDelay(uint64_t previous_ts) {
    return GetCurrentTimeStamp() - previous_ts;
}

////////////////////////////////// Throughput //////////////////////////////////

void BenchmarkTCP_Thr_Out() {
    // TODO send kTotalDataTransmitted bytes in packet of size Packet::kMaxPayloadDataLen (the content of the packet doesnt matter).
    // disable nagle algo car cas d'utilisation avec peu de latence
    boost::asio::io_context io_context;

    boost::asio::ip::tcp::socket socket{io_context};
    boost::system::error_code    ec;

    socket.connect(tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    boost::asio::ip::tcp::no_delay option(true);
    socket.set_option(option);

    std::size_t remaining_data_to_send = kTotalDataTransmitted;

    auto the_mut_buffer = boost::asio::buffer(the_data, flux::Packet::kMaxPayloadDataLen);

    while(remaining_data_to_send > flux::Packet::kMaxPayloadDataLen) {

        std::size_t sent;
        if((sent = socket.send(the_mut_buffer, 0, ec)) == 0) {
            break;
        }

        remaining_data_to_send -= sent;
    }

    socket.send(boost::asio::buffer(the_data, remaining_data_to_send), 0, ec);
}

void BenchmarkUDP_Thr_Out() {
    // TODO send kTotalDataTransmitted bytes in packet of size Packet::kMaxPayloadDataLen (the content of the packet doesnt matter).

    boost::asio::io_context io_context;

    boost::asio::ip::udp::socket socket{io_context};
    boost::system::error_code    ec;

    socket.connect(udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    std::size_t remaining_data_to_send = kTotalDataTransmitted;

    auto the_mut_buffer = boost::asio::buffer(the_data, flux::Packet::kMaxPayloadDataLen);

    while(remaining_data_to_send > flux::Packet::kMaxPayloadDataLen) {

        std::size_t sent;
        if((sent = socket.send(the_mut_buffer, 0, ec)) == 0) {
            break;
        }

        remaining_data_to_send -= sent;
    }

    socket.send(boost::asio::buffer(the_data, remaining_data_to_send), 0, ec);
}

void BenchmarkFLUX_Thr_Out() {
    // TODO send kTotalDataTransmitted bytes in packet of size Packet::kMaxPayloadDataLen (the content of the packet doesnt matter)

    boost::asio::io_context io_context;

    flux::MuxedSock socket{io_context, ENDPOINT_HOSTNAME, TOSTRSTR(ENDPOINT_PORTNO), [](uint16_t the_channel, flux::MuxedSock& the_muxed_sock, flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                               the_data_cb = [](flux::Packet*  the_msg,
                                                flux::SizeType the_data_len) {
                                   // EMPTY
                               };
                           }, HandlePacketLoss};


    std::size_t remaining_data_to_send = kTotalDataTransmitted;

    while(remaining_data_to_send > flux::Packet::kMaxPayloadDataLen) {
        flux::Packet* the_mut_buffer = socket.GetPacket(); // Just alloc dont fill

        socket.Send(0 /* chan zero */, the_mut_buffer, the_mut_buffer->kMaxPayloadDataLen);

        remaining_data_to_send -= flux::Packet::kMaxPayloadDataLen;
    }

    flux::Packet* the_mut_buffer = socket.GetPacket(); // Just alloc dont fill

    socket.Send(0 /* chan zero */, the_mut_buffer, remaining_data_to_send);
}

///////////////////////////////////////

void BenchmarkTCP_Thr_In() {
    // Receive the data and void it.
    // Compute the througput. From new connection to closed connection

    boost::asio::io_context io_context;

    tcp::acceptor acceptor(io_context,
                           tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto                      the_mut_buffer    = boost::asio::buffer(the_data, BUFFERSIZE);
    std::size_t               the_data_received = 0;
    boost::system::error_code ec;

    tcp::socket socket = acceptor.accept();
    // we should start counting time when we start receiving the first packet, not on accept

    auto the_start_time = std::chrono::steady_clock::now();

    for(;;) {

        std::size_t data_size = socket.receive(the_mut_buffer, 0, ec);

        if(data_size == 0) {
            break;
        }

        the_data_received += data_size;
    }

    const auto the_time = (std::chrono::steady_clock::now() - the_start_time).count();

    PrintThr(__FUNCTION__, the_data_received, the_time);
}

void BenchmarkUDP_Thr_In() {
    // Receive the data and void it.
    // Compute the througput. From new connection to closed connection

    boost::asio::io_context io_context;

    udp::socket socket{io_context};
    socket.open(boost::asio::ip::udp::v4());
    socket.bind(udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto                      the_mut_buffer    = boost::asio::buffer(the_data, BUFFERSIZE);
    std::size_t               the_data_received = 0;
    boost::system::error_code ec;
    udp ::endpoint            sender_endpoint;

    // start counting time when we get the first packet
    the_data_received = socket.receive_from(the_mut_buffer, sender_endpoint, 0, ec);

    auto the_start_time = std::chrono::steady_clock::now();

    for(;;) {

        std::size_t data_size = socket.receive_from(the_mut_buffer, sender_endpoint, 0, ec);

        if(data_size == 0 ||
           the_data_received > 0.7 * kTotalDataTransmitted /*janky workaround to udp packet loss*/) {
            break;
        }

        the_data_received += data_size;
    }

    const auto the_time = (std::chrono::steady_clock::now() - the_start_time).count();

    PrintThr(__FUNCTION__, the_data_received, the_time);
}

void BenchmarkFLUX_Thr_In() {
    // Receive the data and void it.
    // Compute the througput. From new connection to closed connection

    boost::asio::io_service the_io_service;

    std::mutex the_barrier;

    decltype(std::chrono::steady_clock::now()) the_start_time;

    std::size_t the_data_received = 0;

    flux::Acceptor an_acceptor{the_io_service,
                               ENDPOINT_HOSTNAME,
                               TOSTRSTR(ENDPOINT_PORTNO),
                               [&the_start_time, &the_data_received, &the_barrier](uint16_t                             the_channel,
                                                                                   flux::MuxedSock&                     the_muxed_sock,
                                                                                   flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                                   // start on new connection/ first packet
                                   the_start_time = std::chrono::steady_clock::now();

                                   the_data_cb = [&the_muxed_sock, &the_data_received, &the_barrier](flux::Packet*  the_msg,
                                                                                                     flux::SizeType the_data_len) {
                                       the_muxed_sock.ReleasePacket(the_msg);
                                       the_data_received += the_data_len;

                                       if(the_data_received > 0.7 * kTotalDataTransmitted /*janky workaround to udp packet loss*/) {
                                           the_barrier.unlock();
                                       }
                                   };
                               },
                               HandlePacketLoss};

    an_acceptor.Run();

    std::thread worker{[&]() {
        the_io_service.run();
    }};

    the_barrier.lock();
    the_barrier.lock();

    the_io_service.stop(); // run fns will return

    worker.join(); // wait for the thread to close

    const auto the_time = (std::chrono::steady_clock::now() - the_start_time).count();

    PrintThr(__FUNCTION__, the_data_received, the_time);
}

//////////////////////////////////// Latency ///////////////////////////////////

void BenchmarkTCP_Lat_Out() {
    // Connect to endpoint and send a small 1 byte packet !
    // wait for the echo
    // disable nagle algo car cas d'utilisation avec peu de latence

    boost::asio::io_context io_context;

    boost::system::error_code      ec;
    boost::asio::ip::tcp::no_delay option(true);

    auto the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));

    for(;;) {
        boost::asio::ip::tcp::socket socket{io_context};

        *reinterpret_cast<uint64_t*>(the_data) = GetCurrentTimeStamp(); // assume same endianess

        socket.connect(tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});
        socket.set_option(option);

        socket.send(the_mut_buffer, 0, ec); // we dont rly need to send the time data..
        socket.receive(the_mut_buffer, 0, ec);

        std::cout << "BenchmarkTCP_Lat_Out : Connection establishment latency + 8 byte send/recv: " << (static_cast<double>(GetDelay(*reinterpret_cast<uint64_t*>(the_data))) / 1000000) << "ms" << std::endl;
        std::this_thread::sleep_for(500ms);
    }
}

void BenchmarkUDP_Lat_Out() {
    // Connect to endpoint and send a small 1 byte packet !
    // wait for the echo

    boost::asio::io_context io_context;

    boost::system::error_code ec;

    auto the_mut_buffer = boost::asio::buffer(the_data, flux::Packet::kMaxPayloadDataLen);

    for(;;) {
        boost::asio::ip::udp::socket socket{io_context};

        *reinterpret_cast<uint64_t*>(the_data) = GetCurrentTimeStamp(); // assume same endianess

        socket.connect(udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

        socket.send(the_mut_buffer, 0, ec); // we dont rly need to send the time data..
        socket.receive(the_mut_buffer, 0, ec);

        std::cout << "BenchmarkUDP_Lat_Out : Connection establishment latency + 8 byte send/recv: " << (static_cast<double>(GetDelay(*reinterpret_cast<uint64_t*>(the_data))) / 1000000) << "ms" << std::endl;

        std::this_thread::sleep_for(500ms);
    }
}

void BenchmarkFLUX_Lat_Out() {
    // Connect to endpoint and send a small 1 byte packet !
    // wait for the echo

    boost::asio::io_service the_io_service;

    decltype(std::chrono::steady_clock::now()) the_start_time;

    for(;;) {
        std::mutex      the_barrier;
        flux::MuxedSock socket{the_io_service,
                               ENDPOINT_HOSTNAME,
                               TOSTRSTR(ENDPOINT_PORTNO),
                               [&the_barrier, &the_io_service](uint16_t                             the_channel,
                                                               flux::MuxedSock&                     the_muxed_sock,
                                                               flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                                   the_data_cb = [&the_barrier, &the_io_service](flux::Packet*  the_msg,
                                                                                 flux::SizeType the_data_len) {
                                       std::cout << "BenchmarkFLUX_Lat_Out : Connection establishment latency + 8 byte send/recv: "
                                                 << (static_cast<double>(GetDelay(*reinterpret_cast<uint64_t*>(the_msg->GetPayloadData()))) / 1000000)
                                                 << "ms" << std::endl;
                                       the_barrier.unlock();
                                   };
                               },
                               HandlePacketLoss};

        auto* packet = socket.GetPacket();

        std::thread worker{[&]() {
            the_io_service.run();
        }};

        *reinterpret_cast<uint64_t*>(packet->GetPayloadData()) = GetCurrentTimeStamp(); // assume same endianess

        socket.Send(0 /* chan zero */, packet, sizeof(uint64_t)); // we dont rly need to send the time data..

        the_barrier.lock();
        the_barrier.lock();

        the_io_service.stop();

        worker.join();

        the_io_service.restart();
        std::this_thread::sleep_for(500ms);
    }
}

///////////////////////////////////////

void BenchmarkTCP_Lat_In() {
    //  accept connection wait for a packet and directly send an echo

    boost::asio::io_context io_context;

    tcp::acceptor acceptor(io_context,
                           tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto                      the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));
    boost::system::error_code ec;

    for(;;) {
        tcp::socket socket = acceptor.accept();

        const std::size_t data_size = socket.receive(the_mut_buffer, 0, ec);
        socket.send(the_mut_buffer, 0, ec);
    }
}

void BenchmarkUDP_Lat_In() {
    //  accept connection wait for a packet and directly send an echo

    boost::asio::io_context io_context;

    udp::socket socket{io_context};
    socket.open(boost::asio::ip::udp::v4());
    socket.bind(udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto                      the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));
    boost::system::error_code ec;
    udp ::endpoint            sender_endpoint;

    for(;;) {

        const std::size_t data_size = socket.receive_from(the_mut_buffer, sender_endpoint, 0, ec);
        socket.send_to(the_mut_buffer, sender_endpoint, 0, ec);
    }
}

void BenchmarkFLUX_Lat_In() {
    //  accept connection wait for a packet and directly send an echo

    boost::asio::io_service the_io_service;

    flux::Acceptor an_acceptor{the_io_service,
                               ENDPOINT_HOSTNAME,
                               TOSTRSTR(ENDPOINT_PORTNO),
                               [](uint16_t                             the_channel,
                                  flux::MuxedSock&                     the_muxed_sock,
                                  flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                                   the_data_cb = [&the_muxed_sock, the_channel](flux::Packet*  the_msg,
                                                                                flux::SizeType the_data_len) {
                                       the_muxed_sock.Send(the_channel, the_msg, the_data_len); // ECHO
                                   };
                               },
                               HandlePacketLoss};

    an_acceptor.Run();

    the_io_service.run();
}

//////////////// Jitter or IP Packet Variation delay ///////////////////////////

void BenchmarkTCP_IPVD_Out() {
    // Send packet each kJitterPacketDelayAsMS ms with the timestamp of the current time of the sending maching (as nano sec ~).
    // disable nagle algo car cas d'utilisation avec peu de latence

    boost::asio::io_context io_context;

    boost::system::error_code      ec;
    boost::asio::ip::tcp::no_delay option(true);

    auto the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));

    boost::asio::ip::tcp::socket socket{io_context};
    socket.connect(tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});
    socket.set_option(option);

    for(;;) {
        if(socket.send(the_mut_buffer, 0, ec) == 0) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(kJitterPacketDelayAsMS));
    }
}

void BenchmarkUDP_IPVD_Out() {
    // Send packet each kJitterPacketDelayAsMS ms with the timestamp of the current time of the sending maching (as nano sec ~).

    boost::asio::io_context io_context;

    boost::system::error_code ec;

    auto the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));

    boost::asio::ip::udp::socket socket{io_context};
    socket.connect(udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    for(;;) {
        if(socket.send(the_mut_buffer, 0, ec) == 0) {
            break;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(kJitterPacketDelayAsMS));
    }
}

void BenchmarkFLUX_IPVD_Out() {
    // Send packet each kJitterPacketDelayAsMS ms with the timestamp of the current time of the sending maching (as nano sec ~).

    boost::asio::io_context io_context;

    flux::MuxedSock socket{io_context, ENDPOINT_HOSTNAME, TOSTRSTR(ENDPOINT_PORTNO), [](uint16_t the_channel, flux::MuxedSock& the_muxed_sock, flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                               the_data_cb = [](flux::Packet*  the_msg,
                                                flux::SizeType the_data_len) {
                                   // EMPTY
                               };
                           },
                           HandlePacketLoss};

    for(;;) {
        flux::Packet* the_mut_buffer                                   = socket.GetPacket(); // Just alloc dont fill

        socket.Send(0 /* chan zero */, the_mut_buffer, sizeof(uint64_t));

        std::this_thread::sleep_for(std::chrono::milliseconds(kJitterPacketDelayAsMS));
    }
}

///////////////////////////////////////

void BenchmarkTCP_IPVD_In() {
    // Store the first received timestamp t0
    // when a second packet is received with a new timestamp t1 do t1 - t0 - kJitterPacketDelayAsMS, t0 = t1
    // repeat

    boost::asio::io_context io_context;

    tcp::acceptor acceptor(io_context,
                           tcp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));

    boost::system::error_code ec;

    tcp::socket socket = acceptor.accept();

    std::chrono::nanoseconds last_time{};
    std::chrono::nanoseconds last_delay{};

    for(;;) {
        const std::size_t data_size = socket.receive(the_mut_buffer, 0, ec);

        if(data_size == 0) {
            break;
        }

        const std::chrono::nanoseconds new_time{GetCurrentTimeStamp()}; // assume same endianess
        const std::chrono::nanoseconds new_delay{(new_time - last_time).count()};

        std::cout << "BenchmarkTCP_IPVD_In : IPDV: " << (std::abs((new_delay - last_delay).count()) / 1E6) << "ms" << std::endl;

        last_time  = new_time;
        last_delay = new_delay;
    }
}

void BenchmarkUDP_IPVD_In() {
    // Store the first received timestamp t0
    // when a second packet is received with a new timestamp t1 do t1 - t0 - kJitterPacketDelayAsMS, t0 = t1
    // repeat

    boost::asio::io_context io_context;

    udp::socket socket(io_context,
                       udp::endpoint{boost::asio::ip::address::from_string(ENDPOINT_HOSTNAME), ENDPOINT_PORTNO});

    auto the_mut_buffer = boost::asio::buffer(the_data, sizeof(uint64_t));

    boost::system::error_code ec;

    std::chrono::nanoseconds last_time{};
    std::chrono::nanoseconds last_delay{};

    for(;;) {
        const std::size_t data_size = socket.receive(the_mut_buffer, 0, ec);

        if(data_size == 0) {
            break;
        }

        const std::chrono::nanoseconds new_time{GetCurrentTimeStamp()}; // assume same endianess
        const std::chrono::nanoseconds new_delay{(new_time - last_time).count()};

        std::cout << "BenchmarkUDP_IPVD_In : IPDV: " << (std::abs((new_delay - last_delay).count()) / 1E6) << "ms" << std::endl;

        last_time  = new_time;
        last_delay = new_delay;
    }
}

void BenchmarkFLUX_IPVD_In() {
    // Store the first received timestamp t0
    // when a second packet is received with a new timestamp t1 do t1 - t0 - kJitterPacketDelayAsMS, t0 = t1
    // repeat

    boost::asio::io_service the_io_service;

    std::chrono::nanoseconds last_time{};
    std::chrono::nanoseconds last_delay{};

    flux::Acceptor an_acceptor{the_io_service,
                               ENDPOINT_HOSTNAME,
                               TOSTRSTR(ENDPOINT_PORTNO),
                               [&last_time, &last_delay](uint16_t                             the_channel,
                                                         flux::MuxedSock&                     the_muxed_sock,
                                                         flux::MuxedSock::OnDataCallbackType& the_data_cb) {
                                   the_data_cb = [&last_time, &last_delay, &the_muxed_sock](flux::Packet*  the_msg,
                                                                                            flux::SizeType the_data_len) {
                                       const std::chrono::nanoseconds new_time{GetCurrentTimeStamp()}; // assume same endianess
                                       const std::chrono::nanoseconds new_delay{(new_time - last_time).count()};

                                       std::cout << "BenchmarkFLUX_IPVD_In : IPDV: " << (std::abs((new_delay - last_delay).count()) / 1E6) << "ms" << std::endl;

                                       last_time  = new_time;
                                       last_delay = new_delay;

                                       the_muxed_sock.ReleasePacket(the_msg);
                                   };
                               },
                               HandlePacketLoss};

    an_acceptor.Run();

    the_io_service.run();
}

////////////////////////////////////////////////////////////////////////////////