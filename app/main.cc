#include <FLUX/acceptor.h>

#include <boost/asio.hpp>
#include <iostream>

#include "benchmark.h"
#include "example1.h"


void fn0() {
    boost::asio::io_service the_io_service;
    flux::Acceptor          an_acceptor{the_io_service,
                               "127.0.0.1",
                               "8080",
                               Echo{},
                               [](uint32_t the_packet_loos_count, flux::MuxedSock&) {
                                   std::cout << the_packet_loos_count << " Packet lost !\n";
                               }};

    an_acceptor.Run();

    the_io_service.run();
}

int main(int argc, char* argv[]) {

    if(argc < 3) {
        fn0();
        // return -1;
    }

    bool is_output = std::string{argv[1]} == std::string{"output"};

    if(std::string{argv[2]}== "tt"){
        is_output ? BenchmarkTCP_Thr_Out() : BenchmarkTCP_Thr_In();
    } else if(std::string{argv[2]}== "ut") {
        is_output ? BenchmarkUDP_Thr_Out() : BenchmarkUDP_Thr_In();
    } else if(std::string{argv[2]}== "ft") {
        is_output ? BenchmarkFLUX_Thr_Out() : BenchmarkFLUX_Thr_In();
    } else if(std::string{argv[2]}== "tl") {
        is_output ? BenchmarkTCP_Lat_Out() : BenchmarkTCP_Lat_In();
    } else if(std::string{argv[2]}== "ul") {
        is_output ? BenchmarkUDP_Lat_Out() : BenchmarkUDP_Lat_In();
    } else if(std::string{argv[2]}== "fl") {
        is_output ? BenchmarkFLUX_Lat_Out() : BenchmarkFLUX_Lat_In();
    } else if(std::string{argv[2]}== "ti") {
        is_output ? BenchmarkTCP_IPVD_Out() : BenchmarkTCP_IPVD_In();
    } else if(std::string{argv[2]}== "ui") {
        is_output ? BenchmarkUDP_IPVD_Out() : BenchmarkUDP_IPVD_In();
    } else if(std::string{argv[2]}== "fi") {
        is_output ? BenchmarkFLUX_IPVD_Out() : BenchmarkFLUX_IPVD_In();
    }

    return 0;
}