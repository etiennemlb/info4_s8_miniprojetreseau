#ifndef BENCHMARK_H
#define BENCHMARK_H

#include <cstdint>

////////////////////////////////////////////////////////////////////////////////

#define TOSTRSTR(s) TOSTR(s)
#define TOSTR(s)    #s

#define ENDPOINT_HOSTNAME "127.0.0.1"
#define ENDPOINT_PORTNO 8080

constexpr std::size_t kTotalDataTransmitted /* In bytes */ = 1024ul * 1024ul * 400ul;

////////////////////////////////// Throughput //////////////////////////////////

// We will try to measure the throughput of payload data (thus not taking into account protocols overhead)

void BenchmarkTCP_Thr_Out();
void BenchmarkUDP_Thr_Out();
void BenchmarkFLUX_Thr_Out();

///////////////////////////////////////

void BenchmarkTCP_Thr_In();
void BenchmarkUDP_Thr_In();
void BenchmarkFLUX_Thr_In();

//////////////////////////////////// Latency ///////////////////////////////////

// We will attempt to measure the latency induced by creating a connection, sending a small packet and waiting for the echo.
// This is the typical connection latency we want to minimize.

void BenchmarkTCP_Lat_Out();
void BenchmarkUDP_Lat_Out();
void BenchmarkFLUX_Lat_Out();

///////////////////////////////////////

void BenchmarkTCP_Lat_In();
void BenchmarkUDP_Lat_In();
void BenchmarkFLUX_Lat_In();

//////////////// Jitter or IP Packet Variation delay ///////////////////////////

constexpr std::size_t kJitterPacketDelayAsMS = 100lu;

// We will attempt to measure the IPVD or jitter (https://tools.ietf.org/html/rfc3393)

void BenchmarkTCP_IPVD_Out();
void BenchmarkUDP_IPVD_Out();
void BenchmarkFLUX_IPVD_Out();

///////////////////////////////////////

void BenchmarkTCP_IPVD_In();
void BenchmarkUDP_IPVD_In();
void BenchmarkFLUX_IPVD_In();

////////////////////////////////////////////////////////////////////////////////

#endif