#include "FLUX/socketenvironment.h"

#include "FLUX/config.h"

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // SocketEnvironment methods definition
    ////////////////////////////////////////////////////////////////////////

    SocketEnvironment::SocketEnvironment(boost::asio::io_service& an_io_service)
        : the_memory_pool_{FLUX_MAX_PACKET_SIZE}
        , the_socket_{an_io_service} {
        // EMPTY
    }

    boost::asio::ip::udp::socket& SocketEnvironment::GetSocket() {
        return the_socket_;
    }

    Packet* SocketEnvironment::AllocPacket() {
        // This is not thread safe !!!!!!!!!!!
        return static_cast<Packet*>(the_memory_pool_.malloc());
    }

    void SocketEnvironment::DeallocPacket(Packet* the_packet) {
        // This is not thread safe !!!!!!!!!!!
        the_memory_pool_.free(the_packet);
    }

} // namespace flux