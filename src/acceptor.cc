#include "FLUX/acceptor.h"

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // Acceptor methods definition
    ////////////////////////////////////////////////////////////////////////

    Acceptor::Acceptor(boost::asio::io_service&                   an_io_service,
                       const std::string&                         the_local_ip,
                       const std::string&                         the_service,
                       const MuxedSock::OnNewChannelCallbackType& the_muxedsock_channel_handler,
                       const MuxedSock::OnPacketLossCallbackType& the_muxedsock_packet_loss_handler)
        : an_io_service{an_io_service}
        , the_socket_{boost::make_shared<SocketEnvironment>(an_io_service)}
        , the_remote_endpoint_{}
        , the_muxedsock_new_channel_handler_{the_muxedsock_channel_handler}
        , the_muxedsock_packet_loss_handler_{the_muxedsock_packet_loss_handler}
        , the_connections_{} {

        boost::asio::ip::udp::resolver        the_resolver(an_io_service);
        boost::asio::ip::udp::resolver::query a_query(boost::asio::ip::udp::v4(), // TODO v4 ou v6
                                                      the_local_ip,
                                                      the_service);

        // Recuperation de la premiere IP
        (*the_socket_).GetSocket().open(boost::asio::ip::udp::v4());
        (*the_socket_).GetSocket().bind(*the_resolver.resolve(a_query));
    }

    void Acceptor::Run() {
        Packet* the_packet = (*the_socket_).AllocPacket();

        if(the_packet == nullptr) {
            return;
        }

        boost::asio::ip::udp::socket& the_socket = (*the_socket_).GetSocket();

        the_socket.async_receive_from(boost::asio::buffer(the_packet->GetRawPacket(), FLUX_MAX_PACKET_SIZE /* (*the_socket_).GetPool().get_requested_size() */),
                                      the_remote_endpoint_,
                                      boost::bind(&Acceptor::HandleConnection,
                                                  this,
                                                  boost::asio::placeholders::error,
                                                  the_packet,
                                                  boost::asio::placeholders::bytes_transferred));
    }

    void Acceptor::HandleConnection(const boost::system::error_code& the_error,
                                    Packet*                          the_packet,
                                    flux::SizeType                   the_packet_len) {

        const Header* the_packet_header;

        if((the_packet_header = Header::ReadHeader(the_packet,
                                                   the_packet_len)) != nullptr) {

            // Valid packet

            MapType::iterator it = the_connections_.find(the_packet_header->the_connection_id_);

            if(it == the_connections_.cend()) {
                auto the_res = the_connections_.insert({the_packet_header->the_connection_id_,
                                                        MuxedSock{the_socket_,
                                                                  the_remote_endpoint_,
                                                                  the_packet_header->the_connection_id_,
                                                                  the_muxedsock_new_channel_handler_,
                                                                  the_muxedsock_packet_loss_handler_}});

                if(!the_res.second) {
                    // TODO
                    return;
                }

                it = the_res.first;
            }

            // We'd like to detach the execution of the channel on data cb so that we dont block the whole server -_-.
            // Forward to the muxedsock
            (*it).second.ProcessPacket(the_packet,
                                       the_packet_len,
                                       the_packet_header,
                                       the_remote_endpoint_);
        }

        Run();
    }

} // namespace flux