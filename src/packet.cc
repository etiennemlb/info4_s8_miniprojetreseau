#include "FLUX/packet.h"

#include <arpa/inet.h>

#include <cstdlib>
#include <cstring>

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // Header methodes definition
    ////////////////////////////////////////////////////////////////////////

    void Header::MakeHeader(Packet*  the_packet,
                            uint64_t the_connection_id,
                            uint16_t the_channel_id_,
                            Flags    the_flags,
                            uint32_t the_data_received) {

        Header* the_header = reinterpret_cast<Header*>(the_packet);

        the_header->the_connection_id_ = hton64b(the_connection_id);
        the_header->the_channel_id_    = htons(the_channel_id_);
        the_header->the_flags_         = htons(the_flags);
        the_header->the_data_received_ = htonl(the_data_received);
    }

    Header* Header::ReadHeader(void*    the_data,
                               uint64_t the_len) {
        uint8_t* const the_data_as_uchar = static_cast<uint8_t*>(the_data);

        if(sizeof(Header) >= the_len) {
            return nullptr;
        }

        Header* the_header = static_cast<Header*>(the_data);

        the_header->the_connection_id_ = ntoh64b(the_header->the_connection_id_);
        the_header->the_channel_id_    = htons(the_header->the_channel_id_);
        the_header->the_flags_         = ntohs(the_header->the_flags_);
        the_header->the_data_received_ = ntohl(the_header->the_data_received_);

        return the_header;
    }

    ////////////////////////////////////////////////////////////////////////
    // FLUXPacket methodes definition
    ////////////////////////////////////////////////////////////////////////

    uint8_t* Packet::GetPayloadData() {
        return the_data + sizeof(Header);
    }

    uint8_t* Packet::GetRawPacket() {
        return the_data;
    }

    Header* Packet::GetHeader() {
        return Header::ReadHeader(GetRawPacket(), sizeof(Packet));
    }

} // namespace flux
