#include "FLUX/muxedsock.h"

#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <iostream>
#include <random>


namespace flux {

    MuxedSock::MuxedSock(boost::asio::io_service&        an_io_service,
                         const std::string&              the_endpoint,
                         const std::string&              the_service,
                         const OnNewChannelCallbackType& the_new_channel_handler,
                         const OnPacketLossCallbackType& the_packet_loss_handler)
        : the_current_end_point_{}
        , the_socket_{boost::make_shared<SocketEnvironment>(an_io_service)}
        , the_channels_{}
        , the_connection_id_{}
        , nb_received_packets{}
        , nb_sent_packets{}
        , the_new_channel_handler_{the_new_channel_handler}
        , the_packet_loss_handler_{the_packet_loss_handler} {

        boost::asio::ip::udp::resolver        the_resolver(an_io_service);
        boost::asio::ip::udp::resolver::query a_query(IP_VERSION == 6 ?
                                                          boost::asio::ip::udp::v6() :
                                                          boost::asio::ip::udp::v4(),
                                                      the_endpoint,
                                                      the_service);

        // Recuperation de la premiere IP
        the_current_end_point_ = *the_resolver.resolve(a_query);

        static std::mt19937                     the_rng(std::random_device{}());
        std::uniform_int_distribution<uint64_t> a_distribution;

        the_connection_id_ = a_distribution(the_rng);

        (*the_socket_).GetSocket().open(IP_VERSION == 6 ? boost::asio::ip::udp::v6() : boost::asio::ip::udp::v4());

        Run();
    }

    MuxedSock::MuxedSock(boost::shared_ptr<SocketEnvironment>& a_socket,
                         boost::asio::ip::udp::endpoint        the_endpoint,
                         uint64_t                              the_connection_id,
                         const OnNewChannelCallbackType&       the_new_channel_handler,
                         const OnPacketLossCallbackType&       the_packet_loss_handler)
        : the_current_end_point_{the_endpoint}
        , the_socket_{a_socket}
        , the_channels_{}
        , the_connection_id_{the_connection_id}
        , nb_received_packets{}
        , nb_sent_packets{}
        , the_new_channel_handler_{the_new_channel_handler}
        , the_packet_loss_handler_{the_packet_loss_handler} {
        // EMPTY
        // Dont launch Run() because the Acceptor will forward packet directly
    }

    bool MuxedSock::Send(uint16_t       the_channel,
                         Packet*        the_packet,
                         flux::SizeType the_payload_len) {

        if(the_payload_len > Packet::kMaxPayloadDataLen) {
            std::cerr << "ERROR: Payload data is too big" << std::endl;
            return false;
        }

        MapType::iterator it = the_channels_.find(the_channel);

        if(it == the_channels_.cend()) {
            OnDataCallbackType the_channel_data_cb;

            the_new_channel_handler_(the_channel,
                                     *this,
                                     the_channel_data_cb);

            auto the_res = the_channels_.insert({the_channel,
                                                 the_channel_data_cb});

            if(!the_res.second) {
                std::cerr << "ERROR: Packet has already been sent" << std::endl;
                return false;
            }

            it = the_res.first;
        }

        Header::MakeHeader(the_packet,
                           the_connection_id_,
                           the_channel,
                           Header::kNone,
                           nb_received_packets);

        the_socket_.get()->GetSocket().send_to(boost::asio::buffer(the_packet->GetRawPacket(), the_payload_len + sizeof(Header)),
                                               the_current_end_point_);

        nb_sent_packets++; // int overflow is tolerated

        ReleasePacket(the_packet);
        return true;
    }

    Packet* MuxedSock::GetPacket() {
        return (*the_socket_).AllocPacket();
    }

    void MuxedSock::ReleasePacket(Packet* the_packet) {
        (*the_socket_).DeallocPacket(the_packet);
    }

    void MuxedSock::ProcessPacket(Packet*                        the_packet,
                                  flux::SizeType                 the_packet_len,
                                  const Header*                  the_packet_header,
                                  boost::asio::ip::udp::endpoint the_endpoint) {

        if(the_packet_header->the_connection_id_ != the_connection_id_) {
            // Not for us !
            return;
        }

        nb_received_packets++;  // int overflow is tolerated

        // handle "ack" field, handle packet losses if different
        if (the_packet_header->the_data_received_ != 0) {
            uint32_t lost_packets = the_packet_header->the_data_received_ - nb_sent_packets;
            if (lost_packets > 0 && lost_packets < FLUX_MAX_LOST_PACKETS) {
                // $lost_packets sent packets were lost!
                the_packet_loss_handler_(lost_packets, *this);
                nb_sent_packets += lost_packets; // reset count difference
            }
        }

        if(the_endpoint != the_current_end_point_) {
            // The endpoint changed (probably from switching network)
            // it could also be an attacker
            std::cout << "WARNING: Endpoint changed from " << the_current_end_point_ << " to " << the_endpoint << std::endl;
            the_current_end_point_ = the_endpoint;
        }

        MapType::iterator it = the_channels_.find(the_packet_header->the_channel_id_);

        if(it == the_channels_.cend()) {
            OnDataCallbackType the_channel_data_cb;

            the_new_channel_handler_(the_packet_header->the_channel_id_,
                                     *this,
                                     the_channel_data_cb);

            auto the_res = the_channels_.insert({the_packet_header->the_channel_id_,
                                                 the_channel_data_cb});

            if(!the_res.second) {
                std::cerr << "ERROR: Could not create a new channel" << std::endl;
                return;
            }

            it = the_res.first;
        }

        // Forward to the OnDataCallbackType
        (*it).second(the_packet,
                     the_packet_len - sizeof(Header));
    }

    void MuxedSock::Run() {
        Packet* the_packet = GetPacket();

        if(the_packet == nullptr) {
            return;
        }

        boost::asio::ip::udp::socket& the_socket = (*the_socket_).GetSocket();

        the_socket.async_receive_from(boost::asio::buffer(the_packet->GetRawPacket(), FLUX_MAX_PACKET_SIZE /* (*the_socket_).GetPool().get_requested_size() */),
                                      the_current_end_point_,
                                      boost::bind(&MuxedSock::HandleConnection,
                                                  this,
                                                  boost::asio::placeholders::error,
                                                  the_packet,
                                                  boost::asio::placeholders::bytes_transferred));
    }

    void MuxedSock::HandleConnection(const boost::system::error_code& the_error,
                                     Packet*                          the_packet,
                                     flux::SizeType                   the_packet_len) {

        const Header* the_packet_header;

        if((the_packet_header = Header::ReadHeader(the_packet,
                                                   the_packet_len)) != nullptr) {

            // Valid packet
            // We'd like to detach the execution of the channel on data cb so that we dont block the whole server -_-.
            ProcessPacket(the_packet,
                          the_packet_len,
                          the_packet_header,
                          the_current_end_point_);
        }

        Run();
    }

} // namespace flux
