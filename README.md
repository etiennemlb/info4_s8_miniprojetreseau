# Tentative d’augmentation de la QoS d’UDP

Paul BLANLOEIL, Étienne MALABOEUF

Nom de code, Project FLUX : Fast Low-latency UDP eXtension

### Introduction

La plupart du trafic généré par les internautes utilise soit TCP soit UDP. Le protocole TCP a été conçu pour être indépendant des données qu’il permet de transporter. TCP tente d’établir une connexion entre deux endpoints. L’établissement et de la connexion et le best effort sur l’envoi des données vient à un coût qui est celui de la bande passante et de la latence, notamment sur les retransmissions où en HTTP par exemple, ça cause des problèmes de type head-of-line blocking). UDP souffre moins de ces problématiques, mais en revanche ne propose pas de mode connecté ou de tentative de renvoi des données.

À travers ce mini projet, nous allons tenter de définir un protocole simpliste minimisant la latence, car si au fil du temps les bandes passantes augmentent, la latence, elle, ne pourra pas être infiniment réduite. Nous utiliserons le multiplexage pour palier a des problèmes annexes comme le nombre de sockets pouvant être ouverts à un moment donné sur une machine donnée (65535 maximum, souvent beaucoup moins), à des problèmes type head-of-line blocking. Nous permettrons à l'utilisateur de connaitre l'état de la connexion à travers la perte de paquet notamment.

Ce projet à un but purement éducatif et devrait nous permettre de gagner en compétence, en compréhension des problématiques temps reel et des avantages/désavantage de chaque protocole. Nous fournirons des benchmarks permettant de visialuser les performances de TCP et d'UDP sur la latence, la gigue et le throughput dans différentes situations de stress (de la connexion).

### Buts

- Maximiser la compatibilité avec le matériel couche 3 et 4
- Minimiser la latence:
  - Établissement de connexion
  - Retransmissions TCP
  - Changements de réseau
- Prévenir l'user des congestions
- [optionel] Préférer le chiffrement par paquet au chiffrement par flux

## Pour compiler :
```
cmake (version 3.10 or more recent)
boost (1.68 or more recent)
gcc > 8 or clang > 9
```

## Compile:

$ mkdir build && cd build

$ cmake -DCMAKE_BUILD_TYPE=[Release|Debug] ..

$ cmake --build . -- -j 16 # or $ make -j 16

## Generate the doc

$ doxygen Doxyfile

The generated doc start at "docs/doxygen/html/index.html"

## Pour faire des tests 

Vous pouvez utiliser le code présent dans `app/*` pour apprendre à utiliser la librairie.
