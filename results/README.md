# Resultats

Avant de commencer le projet, nous nous posions des questions telles que :
 * Quelles contraintes/implication pour du realtime ?
 * Pour du realtime, UDP ou TCP ? Pourquoi ?
 * Comment recoudre/mitiger certains problèmes de latence ?
 * UDP and TCP under load ?

Ce mini projet nous à permis de gagner en compétences, de nous familiariser avec les contraintes de certains systèmes temps réel, de réfléchir aux problèmes de certains protocoles et aux impacts que cela peut avoir en pratique, de comparer TCP et UDP, leurs performances, utilisation avantages et inconvénients.
Sur ces points, nous pensons avoir acquis des compétences.

## Production

Notre petite solution permet de multiplexer l'utilisation d'une socket UDP en connexions et les connexions en channels.
La solution utilise un modèle asynchrone pour la réception des données. La perte de paquets peut être signalée.
Nous avons renoncé à implémenter un système de retransmission ou de dégradation du débit de la connexion pour les raisons suivantes :
 * Un système de retransmission peut être complexe et risque d'introduire des problèmes de head of line blocking. Ce problème (HOL blocking) aurait dû être mitigé par le multiplexage de connexion via les channels.
 * Nous ne dégraderons pas le débit, mais nous signalerons l'utilisateur que des paquets sont perdus. Il pourrait, s’il le souhaite et que son application le permet, dégrader la qualité de son contenu ou ajuster la fréquence d'envoi des paquets.

 Nous avons produit 3 types de benchmarks permettant de calculer le débit : la latence d'établissement d'une connexion, suivie de l'envoi d'un RTT et un benchmark de la gigue.

 Nous avons fait des recherches à propos des contraintes / implications des transmissions temporelles et nous sommes arrivés à la conclusion que sous l'hypothèse du temps réel, il vaut mieux avoir des paquets à jour plutôt que de recevoir tous les paquets, dans le bon ordre.

 Nous avons ensuite mesuré le temps d'établissement d'une connexion TCP, qui est évidemment plus lente qu'un simple RTT UDP. Si l'utilisateur change de réseau, souhaite juste émettre des données, le RTT TCP est un "coût" fixe. Si le cas d'utilisation consiste à créer plein de connexions par plein de machines, TCP n'est pas forcément un bon choix.

 Nous avons continué avec une mesure de la gigue. Les résultats sont flagrants et mettent bien en évidence la sensibilité de TCP aux problèmes de HOL blocking. Les résultats sont visibles dans `benchmark_results.xlsx`. Dans une situation ou 10% des paquets sont perdus, UDP obtient une gigue 2.8 fois meilleure que TCP. Ça se traduirait, disons dans le cas d'un jeu vidéo, par un phénomène de rubber banding, ou l'entité contrôlée par le joueur ferait des sauts dans l'espace (aussi appelé warping ou juste "lag").

 Les tests de latence et de gigue ont été faits avec perte de 10% des paquets et sans perte artificielle.

 Nous avons réalisé des benchmarks de débit. Nous avons pu observer, sous linux, qu'UDP permet des débits plus importants pour la même charge CPU, comparé à TCP. Dans la situation où l'utilisateur a une application sur un LAN, en local, ou dans un data center, il pourrait être envisageable de faire l'hypothèse que les paquets ne sont jamais perdus et utiliser UDP (ou une version avec une très légère couche de retransmission). Cela pourrait permettre de faire des économies d'énergie, ou bien d'envoyer plus de données.

 Avant ce petit projet, nous avions la conviction qu'en situation de perte de paquet, il fallait mieux réduire le débit ou la fréquence d'envoi des paquets. Nous avons testé avec un client utilisant UDP envoyant des paquets à intervalles réguliers sur une connexion "saturée" par un autre client utilisant tout la bande passante pour envoyer des données sur TCP. Nous avons observé deux choses :
 
 - D'une part, UDP semble être défavorisé par rapport à TCP quand il s'agit de router un paquet et que le buffer d'un router est plein,
 - D'autre part, il semblerait qu’il est plus judicieux, dans cette situation de discrimination, que le client UDP envoie plus de paquets (voire même envoyer en double), dans l'espoir qu'un paquet arrive à passer.
 
 Nous trouvons cette situation peu plaisante et contre l'esprit anti-congestion utilisée par TCP (en cas de perte de paquet, temps d'attente exponentiel), et qui permet aujourd’hui de réguler le trafic internet.
 
 Nous fournissons donc une couche au-dessus d'UDP en incorporant des notions de feedback de QoS. Le protocole n'est pas lié à un type de données particulier (vidéo/audio..). Le protocole permet de continuer une connexion même après un changement d'IP (changement de réseau wifi, 3G, 4G…). Le protocole permet de multiplexer une socket en plusieurs channels, à terme il pourrait être envisageable d'ajouter des options par channel pour contrôler comment les données sont envoyées et bufferisées.
 
## Améliorations

Compensation de la gigue ?
Détection de l'ordre d'arriver des paquets ?

Notification à l'utilisateur de la gigue.
Dans ce cas nous nous rapprocherions du Real time Transport Protocol (RTP), mais notons que l'utilisation de ce protocole reste faible tous comme SCTP (https://www.networkworld.com/article/2222277/what-about-stream-control-transmission-protocol--sctp--.html)
