#include <unordered_map>

#include "FLUX/packet.h"
#include "FLUX/socketenvironment.h"

#ifndef FLUX_MUXEDSOCK_H
    #define FLUX_MUXEDSOCK_H

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // MuxedSock definition
    // a socket that can send and receive FLUX packets.
    ////////////////////////////////////////////////////////////////////////

    class Acceptor;

    class MuxedSock {
    public:
        /// Free the packet using MuxedSock::ReleasePacket() or reuse it for Send().
        /// You can not Send() multiple time with the same Packet
        /// You can alloc more packet with GetPacket()
        ///
        using OnDataCallbackType = std::function<void(Packet*,
                                                      flux::SizeType)>;

        using OnNewChannelCallbackType = std::function<void(uint16_t,
                                                            MuxedSock&,
                                                            OnDataCallbackType&)>;

        /// Callback on detection of packet loss (arguments: nb packets lost & this socket)
        /// This is common to all channels for now
        using OnPacketLossCallbackType = std::function<void(uint32_t,
                                                            MuxedSock&)>;


    protected:
        using MapType = std::unordered_map<uint16_t, OnDataCallbackType>;

    public:
        /// Create a new client socket
        ///
        MuxedSock(boost::asio::io_service&        an_io_service,
                  const std::string&              the_endpoint,
                  const std::string&              the_service,
                  const OnNewChannelCallbackType& the_new_channel_handler,
                  const OnPacketLossCallbackType& the_packet_loss_handler);

        /// Create a socket from an already existing socket (from Acceptor mainly)
        ///
        MuxedSock(boost::shared_ptr<SocketEnvironment>& a_socket,
                  boost::asio::ip::udp::endpoint        the_endpoint,
                  uint64_t                              the_connection_id,
                  const OnNewChannelCallbackType&       the_new_channel_handler,
                  const OnPacketLossCallbackType&       the_packet_loss_handler);

        /// Send the_packet.
        /// the_packet must be allocated using GetPacket() or be the packet received by OnDataCallbackType
        /// Returns true if packet has been sent successfully.
        bool Send(uint16_t       the_channel,
                  Packet*        the_packet,
                  flux::SizeType the_payload_len);

        Packet* GetPacket();
        void    ReleasePacket(Packet* the_packet);

    protected:
        /// Process packets forward by Acceptor, or by HandleConnection
        ///
        void ProcessPacket(Packet*                        the_packet,
                           flux::SizeType                 the_packet_len,
                           const Header*                  the_packet_header,
                           boost::asio::ip::udp::endpoint the_endpoint);

        void Run();

        void HandleConnection(const boost::system::error_code& the_error,
                              Packet*                          the_packet,
                              flux::SizeType                   the_packet_len);

        boost::shared_ptr<SocketEnvironment> the_socket_;
        boost::asio::ip::udp::endpoint       the_current_end_point_;  // TODO protect from race condition !

        uint64_t the_connection_id_; // TODO protect from race condition !
        MapType  the_channels_; // TODO protect from race condition !

        OnNewChannelCallbackType the_new_channel_handler_; // TODO protect from race condition !
        OnPacketLossCallbackType the_packet_loss_handler_; // TODO protect from race condition !
        uint32_t nb_received_packets; // TODO protect from race condition !
        uint32_t nb_sent_packets; // TODO protect from race condition !

        friend Acceptor;
    };


} // namespace flux

#endif