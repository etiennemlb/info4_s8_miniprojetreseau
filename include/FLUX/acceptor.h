#include <boost/array.hpp>

#include "FLUX/muxedsock.h"

#ifndef FLUX_ACCEPTOR_H
    #define FLUX_ACCEPTOR_H

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // Acceptor definition
    ////////////////////////////////////////////////////////////////////////

    class Acceptor {
    protected:
        using MapType = std::unordered_map<uint16_t, MuxedSock>;

    public:
        Acceptor(boost::asio::io_service&                   an_io_service,
                 const std::string&                         the_endpoint,
                 const std::string&                         the_service,
                 const MuxedSock::OnNewChannelCallbackType& the_muxedsock_new_channel_handler_,
                 const MuxedSock::OnPacketLossCallbackType& the_muxedsock_packet_loss_handler_);

        void Run();

    protected:
        void HandleConnection(const boost::system::error_code& the_error,
                              Packet*                          the_packet,
                              std::size_t                      the_packet_len);

        boost::asio::io_service& an_io_service;

        boost::shared_ptr<SocketEnvironment> the_socket_;
        boost::asio::ip::udp::endpoint       the_remote_endpoint_;

        MuxedSock::OnNewChannelCallbackType the_muxedsock_new_channel_handler_;
        MuxedSock::OnPacketLossCallbackType the_muxedsock_packet_loss_handler_;

        MapType the_connections_;
    };

} // namespace flux

#endif