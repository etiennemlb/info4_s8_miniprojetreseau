#include "FLUX/config.h"

#ifndef FLUX_PACKET_H
    #define FLUX_PACKET_H

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // Header definition
    ////////////////////////////////////////////////////////////////////////

    class Packet;

    FLUX_PACK_THIS_STRUCT(struct Header {
    protected:
    public:
        enum Flags {
            kNone = 0
        };

        uint64_t the_connection_id_;
        uint16_t the_channel_id_;
        uint16_t the_flags_;
        uint32_t the_data_received_; ///< Cette valeur sert d'ACK si non nulle

    public:
        /// Construit un packet en allouant de la memoire avec malloc
        /// TODO utiliser une pool ? pour eviter les allocations de petite taille
        ///
        static void MakeHeader(Packet * the_packet,
                               uint64_t the_connection_id,
                               uint16_t the_channel_id_,
                               Flags    the_flags,
                               uint32_t the_data_received);

        /// Read
        ///
        static Header* ReadHeader(void*    the_data,
                                  uint64_t the_len);
    });

    static_assert(sizeof(Header) == 16, "Wrong header size");

    ////////////////////////////////////////////////////////////////////////
    // Packet definition
    ////////////////////////////////////////////////////////////////////////

    class Acceptor;
    class MuxedSock;

    FLUX_PACK_THIS_STRUCT(struct Packet {
        static constexpr uint64_t kMaxPayloadDataLen = FLUX_MAX_PACKET_SIZE - sizeof(Header);

        uint8_t* GetPayloadData();

    protected:
        uint8_t* GetRawPacket();

        // Dont call this function more than one time.
        Header* GetHeader();

        uint8_t the_data[FLUX_MAX_PACKET_SIZE];

        friend Acceptor;
        friend MuxedSock;
        friend Header;
    });

} // namespace flux

#endif