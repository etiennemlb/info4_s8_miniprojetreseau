#ifndef FLUX_CONFIG_H
#define FLUX_CONFIG_H

#include <cstdint>

#include <cstddef>

#if !(defined(__GNUC__) || defined(__clang__))
    #error "Compiler not supported. Yes It sucks I know"
#endif

#define FLUX_PACK_THIS_STRUCT(StructDef) StructDef __attribute__((__packed__))

/// Conversion de little endian vers network order ou inversement
/// Ne fonctionne correctement que si l'architecture de la machine
/// hote est en little endian !!
///
#define hton64b(the_int64) (((the_int64)&0xFF00000000000000) >> 56) |     \
                               (((the_int64)&0x00FF000000000000) >> 40) | \
                               (((the_int64)&0x0000FF0000000000) >> 24) | \
                               (((the_int64)&0x000000FF00000000) >> 8) |  \
                               (((the_int64)&0x00000000FF000000) << 8) |  \
                               (((the_int64)&0x0000000000FF0000) << 24) | \
                               (((the_int64)&0x000000000000FF00) << 40) | \
                               (((the_int64)&0x00000000000000FF) << 56)

#define ntoh64b(the_int64) hton64b(the_int64)

////////////////////////////////////////////////////////////////////////////////

namespace flux {
    using SizeType = std::size_t;
} // namespace flux

////////////////////////////////////////////////////////////////////////////////

// Optimal MTU value for the payload of UDP packets to minimise drop rate
#define FLUX_MAX_PACKET_SIZE 1200

// Maximum detectable packet loss, must fit in the data received field
// Allows for safe integer overflow on that field
#define FLUX_MAX_LOST_PACKETS 1 << 31

// IP version to use, either 4 or 6
#define IP_VERSION 4

////////////////////////////////////////////////////////////////////////////////

#endif
