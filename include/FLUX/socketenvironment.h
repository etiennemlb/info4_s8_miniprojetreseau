#include <boost/asio.hpp>
#include <boost/pool/pool.hpp>

#include "FLUX/packet.h"

#ifndef FLUX_SOCKET_H
    #define FLUX_SOCKET_H

namespace flux {

    ////////////////////////////////////////////////////////////////////////
    // SocketEnvironment definition
    ////////////////////////////////////////////////////////////////////////

    class SocketEnvironment {
    public:
        SocketEnvironment(boost::asio::io_service& an_io_service);

        boost::asio::ip::udp::socket& GetSocket();
        Packet*                       AllocPacket();
        void                          DeallocPacket(Packet* the_packet);

    protected:
        boost::pool<boost::default_user_allocator_malloc_free> the_memory_pool_;
        boost::asio::ip::udp::socket                           the_socket_;
    };

} // namespace flux

#endif